import org.example.PlusClass
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PlusClassTest {
  @Test
  fun `1 + 2`() {
    assertEquals(3, PlusClass.plus(1, 2))
  }

  @Test
  fun `1000 + 1000`() {
    assertEquals(2000, PlusClass.plus(1000, 1000))
  }

  @Test
  fun `150 + 150`() {
    assertEquals(300, PlusClass.plus(150, 150))
  }
}